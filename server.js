const { Client, GatewayIntentBits, Partials } = require('discord.js')
require('dotenv').config()

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.MessageContent
  ],
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  fetchAllMembers: true
})

module.exports = client

// load /src/*
const normalizedPath = require('path').join(__dirname, 'src')
require('fs').readdirSync(normalizedPath).forEach(file => require(`./src/${file}`))

client.once('ready', () => console.log('Ready!'))
client.login(process.env.TOKEN)
