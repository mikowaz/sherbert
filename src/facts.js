const client = require('../server')
const config = require('../server.config.json')

client.on('messageCreate', async (message) => {
  if (!config.facts) return
  if (message.author.bot) return

  config.facts.forEach(({ triggerword, text, facts }) => {
    if (message.content.toLowerCase().includes(triggerword)) {
      try {
        // replay with a random fact
        return message.reply(text.replace('%fact%', facts[Math.floor(Math.random() * facts.length)]))
      } catch (e) { console.log(e) }
    }
  })
})
