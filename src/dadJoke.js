const client = require('../server')
const config = require('../server.config.json')

client.on('messageCreate', async (message) => {
  if (!config.dadjoke) return

  const ims = ['i\'m ', 'i am ', 'im ', 'i"m ', 'am ']
  ims.forEach((beginging) => {
    if (message.content.toLowerCase().startsWith(beginging)) {
      if (Math.random() < config.dadjoke.chance) {
        const name = message.content.split(' ').splice(1).join(' ')
        try {
          return message.reply(`Hi ${name}, I'm Sherbert`)
        } catch (e) { console.log(e) }
      }
    }
  })
})
