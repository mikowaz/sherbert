const { EmbedBuilder } = require('discord.js')
const client = require('../server')
const config = require('../server.config.json')

client.on('messageReactionAdd', async (reaction) => {
  if (reaction.partial) {
    try {
      await reaction.fetch()
    } catch (error) {
      console.error('Something went wrong when fetching the message:', error)
      return
    }
  }

  if (reaction._emoji.name === config.starboard.emoji && reaction.count >= config.starboard.count) {
    const channel = client.channels.cache.get(config.starboard.channel)
    const messages = await channel.messages.fetch({ limit: 100 })

    const alreadyPosted = messages.find(message => message?.embeds?.[0]?.footer?.text === reaction.message.id)

    const messageFooter = [
      `${config.starboard.emoji} ${reaction.count}`,
      `in ${client.channels.cache.get(reaction.message.channelId).toString()}`,
      `([to message](${reaction.message.url}))`
    ].join(' ')

    const embed = new EmbedBuilder()
      .setColor(0xFFAC32)
      .setAuthor({ name: reaction.message.author.username, iconURL: reaction.message.author.avatarURL() })
      .setDescription(`${reaction.message.content ? `${reaction.message.content}\n\n` : ''}${messageFooter}`)
      .setFooter({ text: reaction.message.id })
      .setTimestamp(reaction.message.createdTimestamp)

    const attachment = reaction.message.attachments?.first()
    if (attachment) embed.setImage(attachment.url)

    if (alreadyPosted) {
      alreadyPosted.edit({ embeds: [embed] })
    } else {
      channel.send({ embeds: [embed] })
    }
  }
})
