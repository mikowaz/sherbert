const { joinVoiceChannel } = require('@discordjs/voice')
const client = require('../server')
const config = require('../server.config.json')

client.on('ready', () => {
  if (!config.vibevc) return

  const channel = client.channels.cache.get(config.vibevc.channel)
  joinVoiceChannel({
    channelId: channel.id,
    guildId: channel.guild.id,
    adapterCreator: channel.guild.voiceAdapterCreator
  })
})
