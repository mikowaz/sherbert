const client = require('../server')
const config = require('../server.config.json')

client.on('messageReactionAdd', async (reaction) => {
  if (!config.jails) return

  if (reaction.partial) {
    try {
      await reaction.fetch()
    } catch (error) {
      console.error('Something went wrong when fetching the message:', error)
      return
    }
  }

  for (const jail of config.jails) {
    const { emoji, count, jailedrole, immunerole, jailedmessage, backmessage } = jail

    if (reaction._emoji.name === emoji && reaction.count >= count && ((new Date()) - new Date(reaction.message.createdTimestamp)) < 900000) {
      const jailedRole = await reaction.message.guild.roles.fetch(jailedrole)
      const immuneRole = await reaction.message.guild.roles.fetch(immunerole)

      const channel = client.channels.cache.get(reaction.message.channelId)
      const member = await reaction.message.guild.members.fetch(reaction.message.author.id)

      const memberHasJailedRole = member.roles.cache.some(role => role.id === jailedrole)
      const memberHasImmuneRole = member.roles.cache.some(role => role.id === immunerole)

      if (!memberHasJailedRole && !memberHasImmuneRole) {
        member.roles.add(jailedRole)
        channel.send(jailedmessage.replace('%user%', reaction.message.author.username))

        setTimeout(function () {
          member.roles.remove(jailedRole)
          member.roles.add(immuneRole)
          channel.send(backmessage.replace('%user%', reaction.message.author.username))
        }, 300000)

        setTimeout(function () {
          member.roles.remove(immuneRole)
        }, 900000)
      }
    }
  }
})
