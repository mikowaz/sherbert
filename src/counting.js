const client = require('../server')
const config = require('../server.config.json')

client.on('messageCreate', async (message) => {
  if (!config.counting) return

  const channel = client.channels.cache.get(config.counting.channel)
  const messages = await channel.messages.fetch({ limit: 2 })
  const previousMessage = messages.last()

  if (!previousMessage) return console.log('no previous message')
  if (message.channelId !== config.counting.channel) return

  // delete previous message if not a number; message probably edited; should heal the channel
  if (!/^\d+$/.test(previousMessage.content)) {
    try {
      await previousMessage.delete()
      return await message.delete()
    } catch (e) { console.log(e) }
  }

  // delete message if not a number, has the same author as previous, or math doesn't check out
  if (!/^\d+$/.test(message.content) || message.author.id === previousMessage.author.id ||
    +message.content !== (+previousMessage.content + 1)) {
    try {
      return await message.delete()
    } catch (e) { console.log(e) }
  }

  if (+message.content === 9999) {
    channel.send('10000')
    return channel.send('1')
  }

  const funnyNumbersHiHi = [69, 420, 666, 1984, 6969]
  funnyNumbersHiHi.forEach((number) => {
    if (+message.content === number) {
      return channel.send(`${number}`)
    }
  })
})
