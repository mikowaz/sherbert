# SHERBERT

Just another shitty discord bot built with [discord.js](https://discord.js.org/)

---

## How to use

To start the project locally you need a .env file in the root directory of this project.

```dotenv
TOKEN=<INSERT DISCORD BOT TOKEN HERE>
```

then you just run

```bash
npm install
npm start
```

Don't forget to replace the ids and other settings in `server.config.json`.
